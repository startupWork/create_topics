from elasticsearch import Elasticsearch
import configparser
import trigrams_with_significant_keywords

def get_keywordsTeam(teamId) :
    query_keywords = {
                    "query": {
                        "match_all": {}
                    },
                    "aggregations": {
                        "affiliations": {
                            "nested": {
                                "path": "affiliations"
                            },
                            "aggs": {
                                "affiliationfilter": {
                                    "filter": {
                                        "term": {
                                            "affiliations.id": teamId
                                        }
                                    },
                                    "aggs": {
                                        "got_back": {
                                            "reverse_nested": {},
                                            "aggs": {
                                                "most_sig_words": {
                                                    "significant_terms": {
                                                        "field": "title_abstract_sign",
                                                        "size": 100
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
    }
    result = es.search(index=index_pub, doc_type=doc_type_pub, body=query_keywords)
    result = result["aggregations"]["affiliations"]["affiliationfilter"]["got_back"]["most_sig_words"]["buckets"]
    return result

def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

def get_all_resaerchteam(size) :
    query = {"size" : size, "query" : {"match_all" : {}}}
    return es.search(index=index_team, doc_type=doc_type_team, body=query)["hits"]["hits"]

def nb_teams() :
    return es.search(index=index_team, doc_type=doc_type_team, body={"query" : {"match_all" : {}}})["hits"]["total"]

config = configparser.RawConfigParser()
config.read("ConfigFile.properties")
es = Elasticsearch(config.get("elasticsearch", "ip"))
index_team = config.get("elasticsearch", "index_team")
doc_type_team = config.get("elasticsearch", "doc_type_team")
index_pub = config.get("elasticsearch", "index_pub")
doc_type_pub = config.get("elasticsearch", "doc_type_pub")

# keywords = []
nb_teams = nb_teams()
result = get_all_resaerchteam(nb_teams)

for team in result:
    completeText = ""
    if "pubs" in team["_source"] :
        for publication in team["_source"]["pubs"]:
            title_fr = publication["title_fr"]
            title_en = publication["title_en"]
            abstract_fr = ""
            abstract_en = ""
            if "abstract_fr" in publication :
                abstract_fr = publication["abstract_fr"]
            if "abstract_en" in publication :
                abstract_en = publication["abstract_en"]
            completeText += abstract_fr + ' ' + title_fr + ' ' + title_en + ' ' + abstract_en + ' '
            completeText = replace_all(completeText, {"{" : "", "}" : "", '"' : ""} )
        maintopic_data = {}
        data_list = []
        if completeText != "" :
            keywords = get_keywordsTeam(team["_id"])
            data_list = trigrams_with_significant_keywords.create_significant_trigrams(es, completeText, index_pub, keywords)
        maintopic_data["main_topics"] = data_list if data_list else ["empty"]
        es.update(index=index_team, doc_type=doc_type_team, id=team["_id"] ,body={ "doc"  : maintopic_data})

from elasticsearch import Elasticsearch
import configparser
import trigrams_with_significant_keywords

def get_keywordsResearcher(halId) :
    query_keywords = {
                "aggregations": {
                    "all_pub": {
                        "global": {},
                        "aggs": {
                            "pub_filter": {
                                "filter": {
                                    "terms": {
                                        "halId": halId
                                    }
                                },
                                "aggs": {
                                    "most_sig_words": {
                                        "significant_terms": {
                                            "field": "title_abstract_sign",
                                            "size": 100
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
    }
    result = es.search(index=index_pub, doc_type=doc_type_pub, body=query_keywords)
    if "most_sig_words" in result["aggregations"]["all_pub"]["pub_filter"] :
        result = result["aggregations"]["all_pub"]["pub_filter"]["most_sig_words"]["buckets"]
    else :
        result = ""
    return result

def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

def get_author(search_after, es) :
    query = {
        "size" : 1000,
        "query" : {"match_all" : {}}
    }
    return es.search(index=index_author, doc_type=doc_type_author, body=query,scroll="20m")

def nb_raweb_author() :
    return es.search(index=index_author, doc_type=doc_type_author, body={"query" : {"match_all" : {}}})["hits"]["total"]

config = configparser.RawConfigParser()
config.read("ConfigFile.properties")
es = Elasticsearch(config.get("elasticsearch", "ip"))
index_author = config.get("elasticsearch", "index_author")
doc_type_author = config.get("elasticsearch", "doc_type_author")
index_pub = config.get("elasticsearch", "index_pub")
doc_type_pub = config.get("elasticsearch", "doc_type_pub")

total_raweb_author = nb_raweb_author()
range_author = int(total_raweb_author/1000) + 1

for i in range(0, range_author) :
    if i == 0 :
        authors = get_author(i, es)
    else :
        authors = es.scroll(scroll_id=scroll_id, scroll="30m")
    scroll_id = authors["_scroll_id"]
    completeText = ""
    for author in authors["hits"]["hits"] :
        list_pub = []
        completeText = ""
        if "pubs" in author["_source"] :
            for publication in author["_source"]["pubs"]:
                list_pub.append(publication["halId"])
                title_fr = publication["title_fr"]
                title_en = publication["title_en"]
                abstract_fr = ""
                abstract_en = ""
                if "abstract_fr" in publication :
                    abstract_fr = publication["abstract_fr"]
                if "abstract_en" in publication :
                    abstract_en = publication["abstract_en"]
                completeText += abstract_fr + ' ' + title_fr + ' ' + title_en + ' ' + abstract_en + ' '
                completeText = replace_all(completeText, {"{" : "", "}" : "", '"' : ""} )
            maintopic_data = {}
            data_list = []
            if completeText != "" :
                keywords = get_keywordsResearcher(list_pub)
                data_list = trigrams_with_significant_keywords.create_significant_trigrams(es, completeText, index_pub, keywords)
            maintopic_data["main_topics"] = data_list if data_list else ["empty"]
            es.update(index=index_author, doc_type=doc_type_author, id=author["_id"] ,body={ "doc" : maintopic_data})

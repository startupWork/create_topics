import operator

# Gets the trigrams of a given "freq" in a given "text"
def get_trigrams(es, text):
    result_tokens = es.indices.analyze(index="all_publications", analyzer="lookinlabs4hal_bigrams_analyzer", body=text)
    tokens = {}
    for r in result_tokens["tokens"]:
        if r["token"] in tokens :
            nb = tokens[r["token"]]
            tokens[r["token"]] = nb + 1
        else :
            result = is_keywords_exist(es, r["token"].strip())
            if result > 0 :
                distance = 10
                for key, value in tokens.items() :
                    new_dist = levenshtein_distance(r["token"], key)["distance"]
                    if  new_dist < distance :
                        distance = new_dist
                if distance > 4 :
                    tokens[r["token"]] = 1
    sorted_x = sorted(tokens.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    return [a[0] for a in sorted_x]


def is_keywords_exist(es, word) :
    get_all = {
    "size" : 1,
    "query": {
    "bool" :{
    "should" : [
    {"match": {
        "keywords_fr" : word
    }},
    {
     "match": {
         "keywords_en" : word
     }
    }]
    }
    }
    }
    return es.search(index="all_publications", doc_type="publication", body=get_all)["hits"]["total"] > 0

# Returns the levenshtein distance between to strings
def levenshtein_distance(str1, str2):
    m = len(str1)
    n = len(str2)
    lensum = float(m + n)
    d = []
    for i in range(m + 1):
        d.append([i])
    del d[0][0]
    for j in range(n + 1):
        d[0].append(j)
    for j in range(1, n + 1):
        for i in range(1, m + 1):
            if str1[i - 1] == str2[j - 1]:
                d[i].insert(j, d[i - 1][j - 1])
            else:
                minimum = min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + 2)
                d[i].insert(j, minimum)
    ldist = d[-1][-1]
    ratio = (lensum - ldist) / lensum
    return {"distance": ldist, "ratio": ratio}

import nltk
from nltk.collocations import *

# Gets the bigrams of a given "freq" in a given "text"
def get_bigrams(es, text, freq, index_all_pub):
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    result_tokens = es.indices.analyze(index=index_all_pub, body={"analyzer" : "lookinlabs4hal_analyzer", "text": text })
    tokens = []
    for r in result_tokens["tokens"]:
        tokens.append(r["token"])
    finder = BigramCollocationFinder.from_words(tokens)
    if freq is not None:
        finder.apply_freq_filter(freq)
    bigrams = finder.nbest(bigram_measures.likelihood_ratio, 200)
    if bigrams is None :
        bigrams = []
    return bigrams

# Gets the trigrams of a given "freq" in a given "text"
def get_trigrams(es, text, freq, index_all_pub):
    trigram_measures = nltk.collocations.TrigramAssocMeasures()
    result_tokens = es.indices.analyze(index=index_all_pub, body={"analyzer" : "lookinlabs4hal_analyzer", "text": text })
    tokens = []
    for r in result_tokens["tokens"]:
        tokens.append(r["token"])
    finder = TrigramCollocationFinder.from_words(tokens)
    if freq is not None:
        finder.apply_freq_filter(freq)
    trigrams = finder.nbest(trigram_measures.likelihood_ratio, 150)
    if trigrams is None :
        trigrams = []
    return trigrams

# Return a list with the different words of a list of trigrams compared to a given bigram
def get_differences(bigram, ltrigrams):
    differences = []
    for t in ltrigrams:
        if t[0] not in bigram:
            differences.append(t[0])
        if t[1] not in bigram:
            differences.append(t[1])
        if t[2] not in bigram:
            differences.append(t[2])
    return differences

# Removes the duplicates of a list of bigrams
def filter_dup_bigrams(lbigrams):
    items = set()
    result = []
    if lbigrams is not None :
        for b in lbigrams:
            temp = b[0] + ' ' + b[1]
            if b not in result and temp not in items:
                items.add(b[0] + ' ' + b[1])
                items.add(b[1] + ' ' + b[0])
                result.append(b)
    return result

# Removes the bigrams of a list of bigrams that contains any keywords of a list of keywords
def filter_bigrams_by_keywords(lbigrams, lkeywords):
    result = []
    for b in lbigrams:
        add = False
        for k in lkeywords:
            if k["key"] in b:
                add = True
                break
        if add:
            result.append(b)
    return result

# Returns the levenshtein distance between to strings
def levenshtein_distance(str1, str2):
    m = len(str1)
    n = len(str2)
    lensum = float(m + n)
    d = []
    for i in range(m + 1):
        d.append([i])
    del d[0][0]
    for j in range(n + 1):
        d[0].append(j)
    for j in range(1, n + 1):
        for i in range(1, m + 1):
            if str1[i - 1] == str2[j - 1]:
                d[i].insert(j, d[i - 1][j - 1])
            else:
                minimum = min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + 2)
                d[i].insert(j, minimum)
    ldist = d[-1][-1]
    ratio = (lensum - ldist) / lensum
    return {"distance": ldist, "ratio": ratio}

def create_significant_trigrams(es, completeText, index_all_pub, keywords) :
    bigrams = []
    trigrams = []
    trigrams1 = []
    bigrams = get_bigrams(es, completeText, 3, index_all_pub)
    trigrams = get_trigrams(es, completeText, 2, index_all_pub)
    bigrams = filter_dup_bigrams(bigrams)
    if keywords :
        bigrams = filter_bigrams_by_keywords(bigrams, keywords)
    map = {}
    for b in bigrams:
        entry = []
        if b in map:
            entry = map[b]
        for t in trigrams:
            if b[0] in t and b[1] in t:
                entry.append(t)
        if len(entry) > 0:
            map[b] = entry

    second_map = {}
    for key in map:
        values = map[key]
        words = get_differences(key, values)
        entry = [[key[0], key[1]]]
        found = False
        for w1 in words:
            for w2 in words:
                if len(w1) != len(w2):
                    dis = levenshtein_distance(w1, w2)
                    d = dis["distance"]
                    if d <= 3:
                        if len(w1) > len(w2):
                            entry[0].append(w1)
                        else:
                            entry[0].append(w2)
                    found = True
            if found is True:
                break

        if len(entry[0]) > 2:
            second_map[key] = entry
        else:
            second_map[key] = values

    maintopic_data = {}
    maintopic_data["maintopics"] = []
    data = {}
    data["topic"] = []

    added_bigrams = []
    added_trigrams = []
    count = 0
    for b in bigrams:
        if b in second_map:
            t = second_map[b][0]
            exist_similar = False
            for at in added_trigrams:
                coincidences = 0
                if t[0] in at:
                    coincidences += 1
                if t[1] in at:
                    coincidences += 1
                if t[2] in at:
                    coincidences += 1
                if coincidences >= 2:
                    exist_similar = True
                    break
            if t not in added_trigrams and b not in added_bigrams and not exist_similar:
                # maintopic_data["maintopics"].append({"topic": t[0] + ' ' + t[1] + ' ' + t[2]})
                data["topic"].append(t[0] + ' ' + t[1] + ' ' + t[2])
                added_bigrams.append(b)
                added_trigrams.append(t)
                count += 1
    for t in added_trigrams:
        if t[0] == t[1] or t[2] == t[1] or t[0] == t[2]:
            data["topic"].remove(t[0] + ' ' + t[1] + ' ' + t[2])
        for b in bigrams :
            coincidences = 0
            if t[0] in b:
                coincidences += 1
            if t[1] in b:
                coincidences += 1
            if t[2] in b:
                coincidences += 1
            if coincidences >= 2:
                added_bigrams.append(b)

    if count < 15:
        for b in bigrams:
            if count == 15:
                break
            exist_bigram = False
            for ab in data["topic"]:
                if b[0] in ab or b[1] in ab:
                    exist_bigram = True
                    break
                if levenshtein_distance(b[0], ab.split(" ")[0])["distance"] <= 2 or levenshtein_distance(b[0], ab.split(" ")[1])["distance"] <= 2 \
                    or levenshtein_distance(b[1], ab.split(" ")[0])["distance"] <= 2 or levenshtein_distance(b[1], ab.split(" ")[1])["distance"] <= 2 :
                    exist_bigram = True
                    break
            if b not in added_bigrams and not exist_bigram :
            # maintopic_data["maintopics"].append({"topic": t[0] + ' ' + t[1] + ' ' + t[2]})
                data["topic"].append(b[0] + ' ' + b[1])
                added_bigrams.append(b)
                count += 1
    return data["topic"]

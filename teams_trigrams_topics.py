from elasticsearch import Elasticsearch
import configparser
import create_trigrams_keywords

def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

def get_all_resaerchteam(size) :
    query = {"size" : size, "query" : {"match_all" : {}}}
    return es.search(index=index_team, doc_type=doc_type_team, body=query)["hits"]["hits"]

def nb_teams() :
    return es.search(index=index_team, doc_type=doc_type_team, body={"query" : {"match_all" : {}}})["hits"]["total"]

config = configparser.RawConfigParser()
config.read("ConfigFile.properties")
es = Elasticsearch(config.get("elasticsearch", "ip"))
index_team = config.get("elasticsearch", "index_team")
doc_type_team = config.get("elasticsearch", "doc_type_team")
index_all_pub = config.get("elasticsearch", "index_all_pub")
doc_type_all_pub = config.get("elasticsearch", "doc_type_all_pub")

# keywords = []
nb_teams = nb_teams()
result = get_all_resaerchteam(nb_teams)

for team in result:
    completeText = ""
    if "pubs" in team["_source"] :
        for publication in team["_source"]["pubs"]:
            title_fr = publication["title_fr"]
            title_en = publication["title_en"]
            abstract_fr = ""
            abstract_en = ""
            if "abstract_fr" in publication :
                abstract_fr = publication["abstract_fr"]
            if "abstract_en" in publication :
                abstract_en = publication["abstract_en"]
            completeText += abstract_fr + ' ' + title_fr + ' ' + title_en + ' ' + abstract_en + ' '
            completeText = replace_all(completeText, {"{" : "", "}" : "", '"' : ""} )
        maintopic_data = {}
        data_list = []
        if completeText != "" :
            data_list = create_trigrams_keywords.get_trigrams(es, completeText, index_all_pub, doc_type_all_pub)
        maintopic_data["main_topics"] = data_list if data_list else ["empty"]
        es.update(index=index_team, doc_type=doc_type_team, id=team["_id"] ,body={ "doc"  : maintopic_data})
